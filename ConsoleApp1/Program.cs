﻿using System;
using System.Text;

class Program
{

    static void Main()
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;

        // Запитуємо користувача про розмір масиву
        Console.Write("Введіть кількість елементів масиву: ");
        int n = int.Parse(Console.ReadLine());

        // Створюємо масив та заповнюємо його псевдовипадковими числами
        double[] arr = new double[n];
        Random rnd = new Random();
        for (int i = 0; i < n; i++)
        {
            arr[i] = Math.Round(rnd.NextDouble() * (5.103 + 108.005) - 108.005, 3);
        }

        // Знаходимо мінімальний та максимальний елементи масиву
        double min = arr[0];
        double max = arr[0];
        int minIndex = 0;
        int maxIndex = 0;
        for (int i = 1; i < n; i++)
        {
            if (arr[i] < min)
            {
                min = arr[i];
                minIndex = i;
            }
            if (arr[i] > max)
            {
                max = arr[i];
                maxIndex = i;
            }
        }

        // Знаходимо суму індексів елементів, які розташовані між мінімальним та максимальним елементами
        int sumIndex = 0;
        int startIndex = Math.Min(minIndex, maxIndex) + 1;
        int endIndex = Math.Max(minIndex, maxIndex);
        for (int i = startIndex; i < endIndex; i++)
        {
            sumIndex += i;
        }
        Console.WriteLine($"Сума індексів елементів, які розташовані між мінімальним ({min}) та максимальним ({max}) елементами: {sumIndex}");

        // Міняємо порядок розташування елементів, які знаходяться між мінімальним та максимальним елементами, на протилежний
        Array.Reverse(arr, startIndex, endIndex - startIndex);
        Console.WriteLine("Масив після зміни порядку розташування елементів:");
        Console.WriteLine(string.Join(" ", arr));
    }
}
