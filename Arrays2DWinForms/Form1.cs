using System;
using System.Windows.Forms;

namespace Arrays2DWinForms
{
    public partial class Form1 : Form
    {
        double[,] matr;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            int n = (int)col.Value;
            int m = (int)rows.Value;


            matr = new double[n, m];
            Random random = new Random();

            // ���������� ������� ����������������� �������
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    matr[i, j] = Math.Round(random.NextDouble() * (123.908 + 158.403) - 158.403, 3);
                }
            }
            
            

           


            // �������� ������� � DataGridView
            dataGridViewMatrix.ColumnCount = matr.GetLength(1);
            for (int i = 0; i < matr.GetLength(0); i++)
            {
                dataGridViewMatrix.Rows.Add();
                for (int j = 0; j < matr.GetLength(1); j++)
                {
                    dataGridViewMatrix.Rows[i].Cells[j].Value = matr[i, j];
                }
            }
            //��� ������ ������ ����� � �������, �������������� ����� ���:

            for (int j = 0; j < matr.GetLength(0); j++)
                dataGridViewMatrix.Rows[j].HeaderCell.Value = j.ToString();
            //��� ������ ������ ���������� ���������� �� ���������� ������������:
            for (int i = 0; i < matr.GetLength(1); i++)
            {
                dataGridViewMatrix.Columns[i].HeaderText = i.ToString();
                dataGridViewMatrix.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            dataGridViewMatrix.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridViewMatrix_CellPainting);

            /* ��� ����, ��� � ����������� ������������� ��������� ��
           ���������������� ��������, ������� �������� ��䳿 CellPainting � �����
           �����:*/

            void dataGridViewMatrix_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
            {
                if (e.ColumnIndex == -1 && e.RowIndex > -1)
                {
                    e.PaintBackground(e.CellBounds, true);
                    using (SolidBrush br = new SolidBrush(Color.Black))
                    {
                        StringFormat sf = new StringFormat();
                        sf.Alignment = StringAlignment.Center;
                        sf.LineAlignment = StringAlignment.Center;
                        e.Graphics.DrawString(e.RowIndex.ToString(), e.CellStyle.Font, br, e.CellBounds, sf);
                    }
                    e.Handled = true;
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
           
                int n = matr.GetLength(0);
                int m = matr.GetLength(1);

                // ������ �������� �������� � ������� �������. ����� ��� ��������� ���������.
                double[] maxInColumns = new double[m];
                double minMax = double.MaxValue;
                for (int j = 0; j < m; j++)
                {
                    maxInColumns[j] = matr[0, j];
                    for (int i = 1; i < n; i++)
                    {
                        if (matr[i, j] > maxInColumns[j])
                        {
                            maxInColumns[j] = matr[i, j];
                        }
                    }
                    if (maxInColumns[j] < minMax)
                    {
                        minMax = maxInColumns[j];
                    }
                }
            textBox1.Text = minMax.ToString();
        

                
                int k = 2; 
                for (int i = 1; i < n; i += 2) 
                {
                    for (int j = 0; j < k; j++) 
                    {
                        double first = matr[i, 0]; 
                        for (int l = 0; l < m - 1; l++) 
                        {
                            matr[i, l] = matr[i, l + 1];
                        }
                        matr[i, m - 1] = first;
                    }
                }
                // ������� ���� ������� DataGridView
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {
                        dataGridViewMatrix.Rows[i].Cells[j].Value = matr[i, j];
                    }
                }
            

        }
    }
}