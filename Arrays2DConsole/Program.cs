﻿using System;

class Program
{
    static void Main(string[] args)
    {
        Console.Write("Enter the number of rows: ");
        int n = int.Parse(Console.ReadLine());
        Console.Write("Enter the number of columns: ");
        int m = int.Parse(Console.ReadLine());

        double[,] matrix = new double[n, m];
        Random random = new Random();

        // заповнюємо матрицю псевдовипадковими числами
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                matrix[i, j] = Math.Round(random.NextDouble() * (123.908 + 158.403) - 158.403, 3);
            }
        }

        // знаходимо найбільші елементи в кожному стовпці
        double[] maxElements = new double[m];
        for (int j = 0; j < m; j++)
        {
            maxElements[j] = matrix[0, j];
            for (int i = 1; i < n; i++)
            {
                if (matrix[i, j] > maxElements[j])
                {
                    maxElements[j] = matrix[i, j];
                }
            }
        }

        // знаходимо найменший елемент серед найбільших елементів кожного стовпця
        double minMaxElement = maxElements[0];
        for (int j = 1; j < m; j++)
        {
            if (maxElements[j] < minMaxElement)
            {
                minMaxElement = maxElements[j];
            }
        }

        // виводимо результати
        Console.WriteLine("Matrix:");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                Console.Write($"{matrix[i, j],10}");
            }
            Console.WriteLine();
        }
        Console.WriteLine($"Minimum of maximum elements in each column: {minMaxElement}");

        Console.Write("Enter the number of positions to shift: ");
        int k = int.Parse(Console.ReadLine());

        for (int i = 1; i < n; i += 2)
        {
            double[] row = new double[m];
            for (int j = 0; j < m; j++)
            {
                row[j] = matrix[i, j];
            }
            for (int j = 0; j < m; j++)
            {
                matrix[i, j] = row[(j + k) % m];
            }
        }

        // виводимо результати
        Console.WriteLine("Matrix:");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                Console.Write($"{matrix[i, j],10}");
            }
            Console.WriteLine();
        }
    }
}
