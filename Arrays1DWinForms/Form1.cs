namespace Arrays1DWinForms
{
    public partial class Form1 : Form
    {
        double[] arr;
       
        public Form1()
        {

            InitializeComponent();
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
                        System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            int numericUpDown1Count = (int)numericUpDown1.Value;

            double min = arr[0];
            double max = arr[0];
            int minIndex = 0;
            int maxIndex = 0;
            for (int i = 1; i < numericUpDown1Count; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                    minIndex = i;
                }
                if (arr[i] > max)
                {
                    max = arr[i];
                    maxIndex = i;
                }
            }
            int sumIndex = 0;
            int startIndex = Math.Min(minIndex, maxIndex) + 1;
            int endIndex = Math.Max(minIndex, maxIndex);
            for (int i = startIndex; i < endIndex; i++)
            {
                sumIndex += i;
            }
            textBoxSumma.Text = sumIndex.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numericUpDown1Count = (int)numericUpDown1.Value;
            arr = new double[numericUpDown1Count];


            dataGridViewArray.RowCount = 1;
            dataGridViewArray.ColumnCount = numericUpDown1Count;
            Random rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = Math.Round(rnd.NextDouble() * (5.103 + 108.005) - 108.005, 3);
                dataGridViewArray[i, 0].Value = arr[i];
                dataGridViewArray.Columns[i].HeaderText = i.ToString();

                
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridViewArray_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
    }
}
